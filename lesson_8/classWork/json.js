
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <form>
    <input />
    <button></button>
  </form>
  -> '{"name" : !"23123", "age": 15, "password": "*****" }'


*/

document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('convertForm').addEventListener('submit', (e) => {
    e.preventDefault();
    let elements = e.target.elements;
    let result = {};
    for (let i = 0; i < elements.length; i++) {
      let el = elements[i];
      if (el.tagName === 'INPUT') {
        result[el.name] = el.value;
      }
    }
    console.log(JSON.stringify(result));
  });

  document.getElementById('parseForm').addEventListener('submit', (e) => {
    e.preventDefault();
    if (e.target.elements.text.value) {
      console.log(JSON.parse(e.target.elements.text.value));
    }
  });
});
