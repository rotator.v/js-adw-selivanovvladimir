/*
    172.17.13.236:3003/register/ -> POST
        {
            username: «»,
            email: «»
        }

    172.17.13.236:3003/task/ -> POST
        {
            title: «»,
            description: «»,
            user: «»
        }

    172.17.13.236:3003/users/ -> GET
    172.17.13.236:3003/tasks/ -> GET
*/

let urlRegister = 'http://172.17.13.236:3003/register/';
let dataRegister = {
  username: "Name Vova",
  email: "test email",
};
let bodyRegister = JSON.stringify(dataRegister);
let optionsR = {
  method: 'POST',
  headers: {
    "Content-Type": "application/json"
  },
  body: bodyRegister
};

let urlTask = 'http://172.17.13.236:3003/task/';
let bodyTask = JSON.stringify({
  title: 'Vova title',
  description: 'Test description',
  user: 'V user'
});
let optionsTask = {
  method: 'POST',
  headers: {
    "Content-Type": "application/json"
  },
  body: bodyTask
};

let x = fetch(urlRegister, optionsR )
.then( res => res.json() )
.then(res => {
  console.log('res1', res.data);
  document.getElementById('result').innerText += `
      1-st result with id ${res.data._id} created at ${res.data.created_at}
    `;
})
.then(res => {
  return fetch(urlTask, optionsTask)
  .then( res => res.json())
  .then( res => {
    console.log('res2', res.data);
    document.getElementById('result').innerText += `
        2-st result with id ${res.data._id} created at ${res.data.data}
      `;
  })
})
.catch(e => {
  console.log(e);
});
