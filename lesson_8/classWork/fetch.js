/*
  Задача:

  1. При помощи fetch получить данные:
    http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
    http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
    И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

    Подсказка нужно вызвать дополнительный fecth из текущего чейна.
    Для того что бы передать результат выполнения доп. запроса
    в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/

document.addEventListener('DOMContentLoaded', () => {
  let urlData = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
  let urlFriends = 'http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2';

  const getRandomPerson = (data) => {
    let randomNum = random(0, data.length - 1);
    return data[randomNum];
  }
  const random = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  const print = (person) => {
    document.getElementById('result').innerText = `Person with friends:
    id: ${person.index}, name: ${person.name}
    friends:
    ${person.friends.join(', ')}
    `;
  }

  fetch( urlData, { method: 'GET' } )
    .then( res => res.json() )
    .then( getRandomPerson )
    .then( personData => {
      return fetch(urlFriends, { method: 'GET' })
      .then( res => res.json())
      .then( friendsData => {
        let allFriends = personData.friends.concat(friendsData[0].friends)
          .map( value => value.name);
        return {
          index: personData.index,
          name: personData.name,
          friends: allFriends,
        }
      })
    })
  .then( print );

});
