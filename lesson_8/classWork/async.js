/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

document.addEventListener('DOMContentLoaded', () => {

  async function getCompanies() {
    const companiesData = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
    return await companiesData.json();
  }

  const printCompanies = data => {
    let result = document.getElementById('result');

    let res = `<table style="width: 100%;">
      <tr>
        <th>#</th>
        <th>Company</th>
        <th>Balance</th>
        <th>Show registration date</th>
        <th>Show address</th>
      </tr>`;

    data.forEach( (item, index )=> {
      let id = index + 1;
      res += `<tr>
        <td>${id}.</td>
        <td>${item.company}</td>
        <td>${item.balance}</td>
        <td><button id="reg-${id}" data-index="${index}" class="_regDate">Show registration</button></td>
        <td><button id="address-${id}" data-index="${index}" class="_address">Show address</button></td>
      </tr>`;
    });

    res += '</table>';

    result.innerHTML = res;

    result.querySelectorAll('._regDate').forEach(dateBtn => {
      dateBtn.addEventListener('click', (e) => {
        let btn = e.target;
        let btnIndex = btn.dataset.index;
        btn.closest('td').innerText = data[btnIndex].registered;
      });
    });

    result.querySelectorAll('._address').forEach(addressBtn => {
      addressBtn.addEventListener('click', (e) => {
        let btn = e.target;
        let btnIndex = btn.dataset.index;
        let addressKeys = Object.keys(data[btnIndex].address);
        let addressValues = Object.values(data[btnIndex].address);

        btn.closest('td').innerText = addressKeys.map( (key, index) => ` ${key}: ${addressValues[index]}`);
      });
    });
  }

  let companies = getCompanies();
  companies.then(printCompanies);

});