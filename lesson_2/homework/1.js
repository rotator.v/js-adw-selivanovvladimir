
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
        + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
        и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
        Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

let buttons = document.querySelectorAll('.buttonsHeader button.showButton');
let tabs = document.querySelectorAll('.tabContainer .tab');

window.addEventListener('load', function () {
  buttons.forEach(function (btn, i, array) {
    btn.addEventListener('click', clickHandler);
  });
});

function clickHandler(e) {
  hideAllTabs();
  let tabId = e.target.dataset.tab;
  let tab = document.querySelector('.tab[data-tab="' + tabId + '"]');
  tab.classList.add('active');
}

function hideAllTabs() {
  tabs.forEach(function (tab, i, array) {
    tab.classList.remove('active');
  });
}
