/*

  Задание:


    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);


    {
      avatarUrl: 'https://...',
      addLike: function(){...}
    }
    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }

      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>
*/

document.addEventListener('DOMContentLoaded', function () {
  let comment1 = new Comment('Name 1', 'Some comment text');
  let comment2 = new Comment('Name 2', 'Comment 2 some text');
  let comment3 = new Comment('Name 3', 'Comment 3 unique text', 'https://www.w3schools.com/w3images/avatar6.png');
  let comment4 = new Comment('Name 4 test', 'Comment 4 absolutely new text', 'https://www.w3schools.com/w3images/avatar4.png');

  let CommentsArray = [comment1, comment2, comment3, comment4];
  Feed(CommentsArray);

  document.getElementById('addLike1').addEventListener('click', function () {
    comment1.addLike();
  });
  document.getElementById('addLike2').addEventListener('click', function () {
    comment2.addLike();
  });
});

function Comment (name, text, image) {
  this.name = name;
  this.text = text;
  if (image) {
    this.avatarUrl = image;
  }
  this.likes = 0;
  Comment.prototype.id += 1;
  this.id = Comment.prototype.id;
}

function Feed (comments) {
  let feed = document.getElementById('CommentsFeed');
  for (let i = 0; i < comments.length; i++) {
    feed.appendChild(comments[i].getComment());
  }
}

Comment.prototype.id = 0;
Comment.prototype.avatarUrl = 'https://www.w3schools.com/howto/img_avatar.png';
Comment.prototype.addLike = function () {
  this.likes += 1;
  let likeNum = document.querySelector('#comment-' + this.id + ' .likes-num');
  likeNum.innerText = this.likes;
}
Comment.prototype.getComment = function () {
  let result = document.createElement('div');
  result.id = 'comment-' + this.id;
  result.className = 'comment-wrapper';
  result.innerHTML = '<img class="avatar" src="' + this.avatarUrl + '">' +
    '<span class="text">' + this.name + ': ' + this.text + '</span>' +
    '<span class="likes">Likes: <span class="likes-num">' + this.likes + '</span></span>';
  return result;
}