/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)
      decryptCesar1('Sdwq', 5);
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/

window.addEventListener('DOMContentLoaded', function () {
  let textElement = document.getElementById('text');

  document.getElementById('encryptCesar').addEventListener('click', function (e) {
    const encryptCesar1 = encryptCesar.bind(null, textElement.value, 1);
    const encryptCesar2 = encryptCesar.bind(null, textElement.value, 2);
    const encryptCesar3 = encryptCesar.bind(null, textElement.value, 3);
    const encryptCesar4 = encryptCesar.bind(null, textElement.value, 4);
    const encryptCesar5 = encryptCesar.bind(null, textElement.value, 5);

    encryptCesar1();
  });

  document.getElementById('decryptCesar').addEventListener('click', function (e) {
    const decryptCesar1 = encryptCesar.bind(null, textElement.value, -1);
    const decryptCesar2 = encryptCesar.bind(null, textElement.value, -2);
    const decryptCesar3 = encryptCesar.bind(null, textElement.value, -3);
    const decryptCesar4 = encryptCesar.bind(null, textElement.value, -4);
    const decryptCesar5 = encryptCesar.bind(null, textElement.value, -5);

    decryptCesar1();
  });

  function encryptCesar (text, shift) {
    let letters = text.split('');
    let newString = '';
    for (let i = 0; i < letters.length; i++) {
      let newChar = letters[i].charCodeAt() + shift;
      newString += String.fromCharCode(newChar);
    }
    document.getElementById('result').innerText = newString;
  }
});
