/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
window.addEventListener('DOMContentLoaded', function () {
  let train = {
    name: 'My train',
    speed: 100,
    passagers: 0,
    go: function () {
      result.innerHTML += 'Поезд ' + this.name + ' везет ' + this.passagers + ' пассажиров со скоростью ' + this.speed + '<br>';
    },
    stop: function () {
      this.speed = 0;
      result.innerHTML += 'Поезд ' + this.name + ' остановился. Скорость ' + this.speed + '<br>';
    },
    pickup: function (num) {
      this.passagers += num;
      result.innerHTML += 'Поезд ' + this.name + ' теперь везет ' + this.passagers + ' пассажиров.';
    }
  }

  train.go();
  train.stop();
  train.pickup(33);
});
