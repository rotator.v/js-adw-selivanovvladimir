/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
let colors = {
  background: 'purple',
  color: 'white'
}

window.addEventListener('DOMContentLoaded', function () {
  let result = document.getElementById('result');
  let h1 = document.querySelector('h1');

  // myCall.call( colors, 'red' );

  // let binded = myBind.bind(colors);
  // binded();

  myApply.apply(colors, ['Apply function']);

  function myCall( color ){
    result.style.background = this.background;
    result.style.color = color;
    h1.innerText = 'Call function';
  }

  function myBind() {
    result.style.background = this.background;
    result.style.color = this.color;
    h1.innerText = 'I know how binding works in JS';
  }

  function myApply(text) {
    result.style.background = this.background;
    result.style.color = this.color;
    h1.innerText = text;
  }
});

