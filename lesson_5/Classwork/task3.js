/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства


    Dog {
      name: '',
      breed: '',
      status: 'idle',

      changeProp: function( key, value ){...},
      showProps: function(){...}
    }

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/

function Dog (n, b, s) {
  this.name = n;
  this.breed = b;
  this.status = s;
  this.changeProp = function (key, value) {
    this[key] = value;
  }
  this.showProps = function () {
    for (let key in this) {
      let value = this[key];
      if (typeof value !== 'function') {
        console.log(value);
      }
    }
  }
}

let dog = new Dog('Sasha', 'someBreed', null);
console.log('Initial dog', dog);

dog.changeProp('status', 'Dog is running');
console.log('Changed dog', dog);

dog.showProps();
