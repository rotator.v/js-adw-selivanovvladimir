import { Comment } from './comment.js';

const posts = localStorage.getItem('posts') ? JSON.parse(localStorage.getItem('posts')) : {};

const feedWrapper = document.getElementById('feedWrapper');

export const addCommentForm = (e) => {
  const form = document.getElementById('commentForm');
  const parent = e.target.closest('.feed-item');
  let target = parent.querySelector('.form-wrapper');

  let commentForm = document.createElement('form');
  commentForm.innerHTML = form.innerHTML;
  commentForm.className = 'comment-form';
  let parentId = commentForm.querySelector('input#parentId');
  parentId.value = parent.id;

  target.innerHTML = null;
  target.appendChild(commentForm);

  commentForm.addEventListener('submit', (e) => {
    e.preventDefault();
    let f = e.target;
    let author = f.elements.author;
    let text = f.elements.text;
    let postNode = f.closest('.post-item');
    let comment = new Comment(author.value, text.value, postNode.id, f.elements.parentId.value);
    comment.render();
    postNode.querySelector('.comments-wrapper').classList.remove('hidden');
    f.remove();
  });
}

export const showFeed = () => {
  feedWrapper.classList.remove('hidden');
  feedWrapper.innerHTML = null;
  const keys = Object.keys(posts);
  if (!keys.length) {
    feedWrapper.innerHTML = '<h1>No posts found. Add new post!</h1>';
  }
  for (let i = 0; i < keys.length; i++) {
    let post = Post.getPost(keys[i]);
    post.render();
  }
}

export class Post {
  constructor (author, text, imageUrl, id) {
    let date = id ? new Date(+id) : new Date();
    this.id = date.getTime();
    this.created_at = formatDate(date);
    this.author = author;
    this.text = text;
    this.imageUrl = imageUrl;
    this.likes = (posts && posts[this.id]) ? posts[this.id].likes : 0;
    this.comments = (posts && posts[this.id]) ? posts[this.id].comments : [];
    if (!id) {
      this.savePost();
    }
  }

  static getPost(id) {
    if (!posts) {
      return null;
    }
    let postData = posts[id];
    return new Post(postData.author, postData.text, postData.imageUrl, id);
  }

  savePost() {
    posts[this.id] = this;
    localStorage.setItem('posts', JSON.stringify(posts));
  }

  setComment(id) {
    this.comments.push(id);
    this.savePost();
    document.getElementById(this.id).querySelector('.comments span').innerText = this.comments.length;
  }

  addLike() {
    this.likes++;
    this.savePost();
    document.getElementById(this.id).querySelector('.likes span').innerText = this.likes;
  }

  render() {
    let div;
    if (document.getElementById(''+this.id)) {
      div = document.getElementById(''+this.id);
    } else {
      div = document.createElement('div');
      div.id = this.id;
      div.className = 'feed-item post-item';
    }

    div.innerHTML =
      `<div class="item-header">
        <div class="item-author">${this.author}</div>
        <div class="item-date">${this.created_at}</div>
      </div>`;

    let url;
    if (this.imageUrl && this.imageUrl.indexOf('fileKey-') === 0) {
      const fileData = localStorage.getItem(this.imageUrl);
      if (fileData) {
        url = fileData;
      }
    } else {
      url = this.imageUrl;
    }
    if (url) {
      div.innerHTML += `<div class="item-image"><img src="${url}"></div>`;
    }

    const commentsCount = this.comments ? this.comments.length : 0;

    div.innerHTML +=
      `<div class="item-text">${this.text}</div>
      <div class="item-actions">
        <button class="likes">Likes:<span>${this.likes}</span></button>
        <a class="comments" href="#comments">Comments (<span>${commentsCount}</span>)</a>
        <button class="addComment">Write Comment</button>
      </div>
      <div class="form-wrapper"></div>
      <ul class="comments-wrapper hidden"></ul>`;

    let firstChild = feedWrapper.firstChild;
    feedWrapper.insertBefore(div, firstChild);

    if (commentsCount) {
      for (let i = 0; i < this.comments.length; i++) {
        let cid = this.comments[i];
        let comment = Comment.getComment(cid);
        comment.render();
      }
    }

    div.querySelector('.likes').addEventListener('click', () => this.addLike());

    div.querySelector('.comments').addEventListener('click', (e) => {
      e.preventDefault();
      let commentsWrapper = div.querySelector('.comments-wrapper');
      commentsWrapper.classList.toggle('hidden');
    });

    div.querySelector('.addComment').addEventListener('click', addCommentForm);
  }
}

export const formatDate = (date) => {
  return new Date(date.getTime() - (date.getTimezoneOffset() * 60000))
  .toISOString()
  .split(".")[0]
  .replace('T', ' ');
}
