import { formatDate, addCommentForm, Post } from './post.js';

const comments = localStorage.getItem('comments') ? JSON.parse(localStorage.getItem('comments')) : {};

export class Comment {
  constructor (author, text, post, parent, id) {
    let date = id ? new Date(+id) : new Date();
    this.id = date.getTime();
    this.created_at = formatDate(date);
    this.author = author;
    this.text = text;
    this.post = post;
    this.parent = parent ? parent : post;
    if (!id) {
      this.saveComment();
    }
  }

  static getComment(id) {
    if (!comments) {
      return null;
    }
    let data = comments[id];
    return new Comment(data.author, data.text, data.post, data.parent, id);
  }

  saveComment() {
    let post = Post.getPost(this.post);
    post.setComment(this.id);
    comments[this.id] = this;
    localStorage.setItem('comments', JSON.stringify(comments));
  }

  render() {
    let li = document.createElement('li');
    li.id = this.id;
    li.className = 'feed-item comment-item';
    li.innerHTML =
      `<div class="item-header">
        <div class="item-author">${this.author}</div>
        <div class="item-date">${this.created_at}</div>
      </div>
      <div class=item-text"">${this.text}</div>
      <div class="item-actions">
        <button class="addComment">Answer Comment</button>
      </div>
      <div class="form-wrapper"></div>
      <ul class="comments-wrapper"></ul>`;
    li.querySelector('.addComment').addEventListener('click', addCommentForm);

    let wrapper = document.getElementById(this.parent).querySelector('.comments-wrapper');
    wrapper.appendChild(li);
  }
}
