import { Post } from './post';

const urlInput = document.getElementById('imageUrl');
const fileInput = document.getElementById('imageFile');
const setFileKey = file => 'fileKey-' + file.size + '-' + file.name;

export const addPost = () => {
  const formWrapper = document.getElementById('formWrapper');
  formWrapper.classList.remove('hidden');

  const form = document.getElementById('addPost');
  form.querySelector('#uploadImage').addEventListener('click', (e) => {
    e.preventDefault();
    fileInput.click();
  });
  fileInput.addEventListener('change', (e) => {
    if (e.target.files.length) {
      let file = e.target.files[0];
      let url = URL.createObjectURL(file);
      updatePreview(url);
    }
  });

  urlInput.addEventListener('blur', (event) => {
    let link = event.target.value;
    if (link) {
      updatePreview(link);
    }
  });
  urlInput.addEventListener('click', () => {
    if (urlInput.classList.contains('error')) {
      urlInput.value = '';
      urlInput.classList.remove('error');
    }
  });

  form.addEventListener('submit', (e) => {
    e.preventDefault();

    let processForm = true;
    let url = null;
    const imageLoaded = (fileInput.files.length || urlInput.value);
    if (imageLoaded && fileInput.files.length) {
      let file = fileInput.files[0];
      processForm = false;
      url = setFileKey(file);

      let reader = new FileReader();
      reader.onload = () => {
        localStorage.setItem(url, reader.result);
        submitForm(form, url);
      };
      reader.readAsDataURL(file);
      return;
    }
    else if (imageLoaded && urlInput.value) {
      if (urlInput.classList.contains('error')) {
        processForm = false;
      } else {
        url = urlInput.value;
      }
    }

    if (processForm) {
      submitForm(form, url);
    }
  });
}

const updatePreview = (fileUrl) => {
  const preview = document.querySelector('.image-file .image-preview');
  const image = new Image();
  image.onload = () => {
    preview.innerHTML = null;
    preview.appendChild(image);
  };
  image.onerror = () => {
    urlInput.classList.add('error');
    urlInput.value = 'Image can\'t be loaded by this URL';
  }
  image.src = fileUrl;
}

const submitForm = (form, url) => {
  let post = new Post(form.elements.author.value, form.elements.text.value, url);
  post.render();
  form.reset();
}
