import { addPost } from './modules/addPost.js';
import { showFeed } from './modules/post.js';

const app = document.getElementById('app');
const feedWrapper = document.getElementById('feedWrapper');
const formWrapper = document.getElementById('formWrapper');

const routerHash = '#/';
const handleNav = (hash) => {
  if (hash.indexOf(routerHash) !== -1) {
    let route = hash.replace(routerHash, '');
    switch (route) {
      case 'news':
        formWrapper.classList.add('hidden');
        showFeed();
        break;

      case 'add-post':
        feedWrapper.classList.add('hidden');
        addPost();
        break;

      default:
        location.hash = '#/not-found';
        app.innerHTML = '<h1>Page not found.</h1>';
    }
  } else {
    showFeed();
    addPost();
  }
}

document.querySelectorAll('a').forEach(link => {
  link.addEventListener('click', (e) => {
    handleNav(e.target.hash, e);
  });
});

window.addEventListener('load', handleNav(location.hash));

window.addEventListener('popstate', event => handleNav(event.target.location.hash));
