const gulp = require('gulp'),
  gulpIf = require('gulp-if'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  concat = require('gulp-concat'),
  del = require('del'),
  imagemin = require('gulp-imagemin'),
  rename = require('gulp-rename'),
  uglify = require('gulp-uglify'),
  webpack = require('webpack'),
  webpackStream = require('webpack-stream');

const isProd = process.env.NODE_ENV === 'production';

// Clean tasks.
gulp.task('clean:img', () => {
  return del(['public/images', 'gulp/tmp']);
});
gulp.task('clean:css', () => {
  return del(['public/css', 'gulp/tmp']);
});
gulp.task('clean:js', () => {
  return del(['public/js', 'gulp/tmp']);
});
gulp.task('clean', () => {
  return del(['public/js', 'public/css', 'public/images', 'gulp/tmp']);
});

// Compile styles.
gulp.task('sass', () => {
  return gulp.src('app/scss/style.scss')
  .pipe(autoprefixer({
    cascade: false
  }))
  .pipe(sass({
    sourceComments: false,
    outputStyle: 'compressed'
  }))
  .pipe(concat('style.min.css'))
  .pipe(gulp.dest('public/css'));
});

// Minify images
gulp.task('images', () =>
  gulp.src('app/images/*')
  .pipe(imagemin({ verbose: true }))
  .pipe(gulp.dest('public/images/'))
);

// JS with webpack
gulp.task('js', () => {
  return gulp.src('app/index.js')
  .pipe(webpackStream({
    mode: isProd ? 'production' : 'development',
    devtool: isProd ? false : 'inline-source-map',
    output: {
      filename: 'app.js',
    },
    module: {
      rules: [
        {
          test: /\.(js)$/,
          exclude: /(node_modules)/,
          loader: 'babel-loader',
          query: {
            presets: ['env']
          }
        }
      ]
    }
  }))
  .pipe(gulp.dest('./public/js'))
  .pipe(gulpIf(isProd, uglify()))
  .pipe(gulpIf(isProd, rename({ suffix: '.min' })))
  .pipe(gulpIf(isProd, gulp.dest('./public/js')));
});

gulp.task('default', gulp.series('clean', 'images', 'sass', 'js'));

gulp.task('watch', () => {
  gulp.watch('app/**/*.scss', gulp.series('clean:css', 'sass'));
  gulp.watch('app/**/*.js', gulp.series('clean:js', 'js'));
  gulp.watch('app/images/*.*', gulp.series('clean:img', 'images'));
});
