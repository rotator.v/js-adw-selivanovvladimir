/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678

*/

let loginForm = document.getElementById('loginForm');
let userData = document.getElementById('userData');

const login = () => {
  localStorage.setItem('authorized', true);
  loginForm.style.display = 'none';
  userData.style.display = 'block';
  document.getElementById('userHello').innerHTML = `Hello, ${localStorage.getItem('username')}`;
  document.querySelectorAll('#loginForm input').forEach(item => {
    item.value = null;
  });
  document.getElementById('errorMessage').innerHTML = null;
}

const logout = () => {
  localStorage.removeItem('authorized');
  loginForm.style.display = 'block';
  userData.style.display = 'none';
  document.getElementById('userHello').innerHTML = null;
}

let localUsername = localStorage.getItem('username');
let auth = localStorage.getItem('authorized');
if (localUsername && auth) {
  login();
}

loginForm.addEventListener('submit', (e) => {
  e.preventDefault();
  let localUsername = localStorage.getItem('username');
  let localPass = localStorage.getItem('pass');

  let elements = e.target.elements;
  let name = elements.username.value;
  let pass = elements.password.value;

  let valid = localUsername === name && localPass === pass;

  if (localUsername && localPass) {
    if (valid) {
      login();
    } else {
      document.getElementById('errorMessage').innerText = 'You enter incorrect login or password.';
    }
  } else {
    localStorage.setItem('username', name);
    localStorage.setItem('pass', pass);
    login();
  }
});

document.getElementById('logout').addEventListener('click', () => {
  if (localStorage.getItem('authorized')) {
    logout();
  }
});

document.getElementById('deleteAccount').addEventListener('click', () => {
  localStorage.removeItem('username');
  localStorage.removeItem('pass');
  logout();
});
