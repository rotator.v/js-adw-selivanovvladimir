/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/

let color = localStorage.getItem('backgroundColor');
if (color) {
  document.body.style.backgroundColor = color;
}

document.getElementById('randomColor').addEventListener('click', () => {
  let newColor = getColor();
  document.body.style.backgroundColor = newColor;
  localStorage.setItem('backgroundColor', newColor);
});

const getColor = () => {
  let color = '#';
  for (let i=0; i<3; i++) {
    let rand = getRandomIntInclusive(0, 255);
    color += rand.toString(16);
  }
  return color;
}

const getRandomIntInclusive = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
