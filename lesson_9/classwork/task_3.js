/*

    Задание 3:

    Написать класс Posts в котором есть данные:

    _id
    isActive,
    title,
    about,
    likes,
    created_at

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было прездагрузить данные в данный модуль: http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2

*/

let posts = localStorage.getItem('posts') ? JSON.parse(localStorage.getItem('posts')) : {};

class Posts {
  constructor (id, title, about, created, active) {
    this._id = id;
    this.isActive = active;
    this.title = title;
    this.created_at = created;
    this.about = about;
    this.like = (posts && posts[id]) ? posts[id].like : 0;

    this.savePost();
  }

  render() {
    let post = document.createElement('div');
    post.className = 'post-wrapper';
    post.id = this._id;
    post.innerHTML = `<div>
      <h4 class="title">${this.title}</h4>
      <div class="info">
        <span>Date: ${this.created_at}</span>
        <span>Status: ${this.isActive ? 'Active' : 'Not active'}</span>
      </div>
      <div class="likes">Likes: <span>${this.like}</span><button id="likeIt">Like!</button></div>
      <div class="text">${this.about}</div>
    </div>`;
    document.getElementById('news').appendChild(post);

    post.querySelector('#likeIt').addEventListener('click', () => {
      this.addLike();
      console.log('like');
    });
  }

  savePost() {
    posts[this._id] = this;
    localStorage.setItem('posts', JSON.stringify(posts));
  }

  addLike() {
    this.like++;
    this.savePost();
    document.getElementById(this._id).querySelector('.likes span').innerText = this.like;
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const print = data => {
    let keys = Object.keys(data);
    for (let i = 0; i < keys.length; i++) {
      let prop = keys[i];
      let item = data[prop];
      let post = new Posts(item._id, item.title, item.about, item.created_at, item.isActive);
      post.render();
    }
  }

  if (localStorage.getItem('posts')) {
    posts = JSON.parse(localStorage.getItem('posts'));
    print(posts);
    console.log('loaded');
  } else {
    fetch( 'http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2')
    .then( res => res.json() )
    .then( print );
    console.log('new');
  }
});
