

    /*

        Документация:
        
        https://developer.mozilla.org/ru/docs/HTML/HTML5/Constraint_validation
        
        form.checkValidity() > Проверка всех полей формы на валидость
        form.reportValidity() > Проверяет все поля на валидность и выводит возле каждого из не прошедшего валидацию
            сообщение с ошибкой

        formElement.validity > Объект с параметрами валидности поля 
        formElement.setCustomValidity(message) > Метод который задаст validity.valid = false, и при попытке отправки
            сообщения выведет message в браузерный попал

        Классы для стилизации состояния элемента
        input:valid{}
        input:invalid{}

        Задание:
        Используя браузерное API для валидации форм реализовать валидацию следующей формы:

        - Имя пользователя: type:text -> validation: required; minlength = 2;  
            Если пустое выввести сообщение: "Как тебя зовут дружище?!"
        - Email: type: email -> validation: required; minlength = 3; validEmail;
            Если эмейл не валидный вывести сообщение "Ну и зря, не получишь бандероль с яблоками!"
        - Пароль: type: password -> validation: required; minlength = 8; maxlength=16;
            Если пустой вывести сообщение: "Я никому не скажу наш секрет";
        - Количество сьеденых яблок: type: number -> validation: required; minlength = 1; validNumber;
            Если количество 0 вывести эррор с сообщением "Ну хоть покушай немного... Яблочки вкусные"
        - Напиши спасибо за яблоки: type: text -> validation: required; 
            Если текст !== "спасибо" вывести эррор с сообщением "Фу, неблагодарный(-ая)!" используя setCustomValidity();

        - Согласен на обучение: type: checkbox -> validation: required;
            Если не выбран вывести эррор с сообщение: "Необразованные живут дольше! Хорошо подумай!"

        Внизу две кнопки:

        1) Обычный submit который отправит форму, если она валидна.
        2) Кнопка Validate(Проверить) которая запускает методы:
            - yourForm.checkValidity: и выводит на страницу сообщение с результатом проверки
            - yourForm.reportValidity: вызывает проверку всех правил и вывод сообщения с ошибкой, если такая есть

    */

    window.addEventListener('DOMContentLoaded', function () {
      let form = document.getElementById('ValidationForm');
      let username = form.elements.username;
      let email = form.elements.email;
      let pass = form.elements.pass;
      let appleNum = form.elements.appleNum;
      let thanks = form.elements.thanks;
      let agree = form.elements.agree;

      username.addEventListener('input', function () {
        checkMissing(username, 'Как тебя зовут дружище?!');
      });
      email.addEventListener('input', function () {
        checkEmail();
      });
      pass.addEventListener('input', function () {
        checkMissing(pass, 'Я никому не скажу наш секрет');
      });
      appleNum.addEventListener('input', function () {
        checkAppleNum();
      });
      thanks.addEventListener('input', function () {
        checkThanks();
      });
      agree.addEventListener('change', function () {
        checkAgree();
      });

      document.getElementById('validate').addEventListener('click', function (e) {
        e.preventDefault();

        checkMissing(username, 'Как тебя зовут дружище?!');
        checkEmail();
        checkMissing(pass, 'Я никому не скажу наш секрет');
        checkAppleNum();
        checkThanks();
        checkAgree();

        result.innerHTML = form.checkValidity() ? 'Form is valid' : 'Form NOT valid';
        form.reportValidity();
      });

      function checkMissing (elem, text) {
        if (elem.validity.valueMissing) {
          elem.setCustomValidity(text);
        } else {
          elem.setCustomValidity('');
        }
      }
      function checkEmail () {
        if (email.validity.typeMismatch) {
          email.setCustomValidity('Ну и зря, не получишь бандероль с яблоками!');
        } else {
          email.setCustomValidity('');
        }
      }
      function checkAppleNum() {
        if (appleNum.value === '0') {
          appleNum.setCustomValidity('Ну хоть покушай немного... Яблочки вкусные');
        } else {
          appleNum.setCustomValidity('');
        }
      }
      function checkThanks() {
        if (thanks.value !== 'спасибо') {
          thanks.setCustomValidity('Фу, неблагодарный(-ая)!');
        } else {
          thanks.setCustomValidity('');
        }
      }
      function checkAgree() {
        if (!agree.checked) {
          agree.setCustomValidity('Необразованные живут дольше! Хорошо подумай!');
        } else {
          agree.setCustomValidity('');
        }
      }
    });